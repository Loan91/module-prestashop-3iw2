<?php
// Table des commandes des livreurs
$db = Db::getInstance()->Execute("CREATE TABLE "._DB_PREFIX_."commande_livreur (id_commande_livreur INT(10) NOT NULL AUTO_INCREMENT , id_order INT(10) NOT NULL , id_employee INT(10) NOT NULL , PRIMARY KEY (id_commande_livreur))");

// Ajout du rôle Livreur
$db = Db::getInstance()->insert('profile', array(
    'id_profile' => 10,
));

// Ajout de la traduction du rôle
$db = Db::getInstance()->insert('profile_lang', array(
    'id_lang' => 1,
    'id_profile' => 10,
    'name' => 'Livreur',
));

// Ajout du transporteur (livreur)
$db = DB::getInstance()->insert('carrier', array(
    'id_carrier' => 10,
    'id_reference' => 10,
    'name' => 'Livraison à domicile',
    'active' => 1,
    'is_free' => 1,
    'external_module_name' => 'deliveryfood',
));

// Ajout du transporteur dans les groupes principaux
$db = DB::getInstance()->insert('carrier_group', array(
    'id_carrier' => 10,
    'id_group' => 1,
));
$db = DB::getInstance()->insert('carrier_group', array(
    'id_carrier' => 10,
    'id_group' => 2,
));
$db = DB::getInstance()->insert('carrier_group', array(
    'id_carrier' => 10,
    'id_group' => 3,
));

// Ajout de la traduction du transporteur
$db = DB::getInstance()->insert('carrier_lang', array(
    'id_carrier' => 10,
    'id_shop' => 1,
    'id_lang' => 1,
    'delay' => 'Livraison à domicile en 30 à 45 minutes',
));

// Ajout de la zone de livraison du transporteur
$db = DB::getInstance()->insert('carrier_zone', array(
    'id_carrier' => 10,
    'id_zone' => 1,
));

// Ajout du mode de livraison dans la boutique
$db = DB::getInstance()->insert('delivery', array(
    'id_delivery' => 10,
    'id_carrier' => 10,
    'id_zone' => 1,
    'price' => 0.0,
));

// Link du transporteur à la boutique
$db = DB::getInstance()->insert('carrier_shop', array(
    'id_carrier' => 10,
    'id_shop' => 1,
));

// Ajout du lien rapide de l'administration deliveryfood dans les liens d'accès rapide
$db = DB::getInstance()->insert('quick_access', array(
    'id_quick_access' => 10,
    'link' => 'index.php/improve/modules/manage/action/configure/deliveryfood',
));

// Ajout de la traduction de l'accès rapide
$db = DB::getInstance()->insert('quick_access_lang', array(
    'id_quick_access' => 10,
    'id_lang' => 1,
    'name' => 'Administration DeliveryFood',
));