# 3IW2 Prestashop Module

The purpose of this project is to create a Prestashop module for sales and delivery in the catering sector.

## Description

This module was created in order to satisfy the demands of restaurants in terms of deliveries via their online site.

It provides a new option for home delivery after validation of a customer basket.

The site administrator will receive a notification for each order received for home delivery.

He must then validate the order, submit an estimate of time for delivery and assign a delivery person.

The customer will then receive a notification with the information of his order.

Once the service is completed, the administrator will have access to a summary of the deliveries made for each delivery person.

This module will offer:
- order management for home delivery
- management of deliverers
- cash management after each service

## Installation

Drop the module folder inside the Prestashop module folder.

## Usage

Coming soon...
