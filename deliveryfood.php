<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class DeliveryFood extends Module
{
    public function __construct()
    {
        $this->name = 'deliveryfood';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Groupe 5';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Delivery Food');
        $this->description = $this->l('Manage deliveries at home easily.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (!parent::install() ||
            !$this->registerHook('leftColumn') ||
            !$this->registerHook('header') ||
            !Configuration::updateValue('MYMODULE_NAME', 'deliveryfood')
        ) {
            return false;
        }

        // Setup de la base de données
        include 'setupbdd.php';

        return true;
    }

    public function getContent()
    {
        // Si les champs commande et livreur du formulaire sont remplis (formulaire d'une commande)
        if (isset($_POST['commande']) && isset($_POST['livreur']))
            // Ajout de la commande assignée dans la BDD
            $db = Db::getInstance()->insert('commande_livreur', array(
                'id_order' => $_POST['commande'],
                'id_employee' => $_POST['livreur']
            ));

        // Retourne la liste des livreurs + la liste des commandes non assignées + la liste des commandes assignées + la liste des caisses
        return $this->listeLivreurs() . $this->listeCommandesNonAssignees() . $this->listeCommandesAssignees() . $this->totalCaisseLivreurs();
    }

    public function listeLivreurs()
    {
        $text = "<div class='panel'><div class='panel-heading'>Liste des livreurs</div>";

        // Liste des livreurs
        $sql = 'SELECT * FROM '._DB_PREFIX_.'employee WHERE id_profile = 10 ORDER BY lastname, firstname ASC';
        // S'il y a au moins un livreur
        if ($livreurs = Db::getInstance()->ExecuteS($sql))
            foreach ($livreurs as $livreur)
                $text .= '- ' . $livreur['firstname'] . ' ' . $livreur['lastname']. ' (' . $livreur['email'] .')<br />';
        else
            $text .= "Aucun livreur.";
        $text .= "</div>";

        return $text;
    }

    public function listeCommandesNonAssignees()
    {
        // Récupération des commandes non assignées du jour
        $sql = 'SELECT * FROM '._DB_PREFIX_.'orders WHERE id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'commande_livreur) AND id_carrier = 10 AND date_add BETWEEN "'. date('Y-m-d 00:00:00'). '" AND "'. date('Y-m-d 23:59:59') . '"';

        // Nombre de commandes non assignées
        $nbCommandes = Db::getInstance()->ExecuteS('SELECT count(id_order) as nb FROM '._DB_PREFIX_.'orders WHERE id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'commande_livreur) AND id_carrier = 10 AND date_add BETWEEN "'. date('Y-m-d 00:00:00'). '" AND "'. date('Y-m-d 23:59:59') . '"');

        $text = "<div class='panel'><div class='panel-heading'>Liste des commandes non assignées ( ". $nbCommandes[0]['nb'] ." )</div>";

        // S'il y a au moins une commande non assignée
        if ($commandes = Db::getInstance()->ExecuteS($sql)) {
            // Liste des livreurs
            $sql = 'SELECT * FROM '._DB_PREFIX_.'employee WHERE id_profile = 10 ORDER BY lastname, firstname ASC';
            $livreurs = Db::getInstance()->ExecuteS($sql);

            foreach ($commandes as $commande){
                $text .= '- Commande n°' . $commande['id_order'] . " (Réf. " . $commande['reference'] . ') à '. date('H:i:s', strtotime($commande['date_add'])) .' : '.
                            '<form style="display: inline" method="POST">
                                <input type="hidden" name="commande" value="'.$commande['id_order'].'">
                                <select style="width: 200px; display: inline;" name="livreur">';
                                    foreach ($livreurs as $livreur)
                                            $text .= '<option value="'.$livreur['id_employee'].'">'.$livreur['firstname'].' ' . $livreur['lastname']. '</option>';
                        $text .= '</select> 
                                <input style="display:inline;" type="submit" value="Assigner">
                            </form><br>';
                
                // Récupération des articles de la commande non assignée
                $sql = 'SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = '. $commande['id_order'];

                $totalCommande = 0.0; // Montant total de la commande non assignée

                // S'il y a au moins un article
                if ($articles = Db::getInstance()->ExecuteS($sql))
                    foreach ($articles as $article)
                        $text .= "<span style='padding-left: 20px;'></span>* ".$article['product_quantity'].'x '.$article['product_name']. " (" . number_format(($article['unit_price_tax_incl']*$article['product_quantity']), 2). "€)<br>";
                else
                    $text .= "Erreur lors de l'affichage de l'article";

                $totalCommande += number_format($commande['total_products'], 2);

                $text .= "<span style='padding-left: 8px;'></span>Total : ".$totalCommande."€<br>". (!next( $commandes ) ? "" : "<br>" );
            }
        } else {
            $text .= "Aucune commande non assignée.";
        }

        $text .= "</div>";
        return $text;
    }

    public function listeCommandesAssignees()
    {
        // Récupération des commandes assignées
        $sql = 'SELECT * FROM '._DB_PREFIX_.'orders WHERE id_order IN (SELECT id_order FROM '._DB_PREFIX_.'commande_livreur) AND id_carrier = 10 AND date_add BETWEEN "'. date('Y-m-d 00:00:00'). '" AND "'. date('Y-m-d 23:59:59') . '"';

        // Nombre de commandes assignées
        $nbCommandes = Db::getInstance()->ExecuteS('SELECT count(id_order) as nb FROM '._DB_PREFIX_.'orders WHERE id_order IN (SELECT id_order FROM '._DB_PREFIX_.'commande_livreur) AND id_carrier = 10 AND date_add BETWEEN "'. date('Y-m-d 00:00:00'). '" AND "'. date('Y-m-d 23:59:59') . '"');

        $text = "<div class='panel'><div class='panel-heading'>Liste des commandes assignées ( ". $nbCommandes[0]['nb'] ." )</div>";

        // S'il y a au moins une commande assignée
        if ($commandes = Db::getInstance()->ExecuteS($sql)) {
            foreach ($commandes as $commande){
                // Récupération du livreur assigné
                $sql = 'SELECT * FROM '._DB_PREFIX_.'employee, '._DB_PREFIX_.'commande_livreur WHERE '._DB_PREFIX_.'commande_livreur.id_employee = '._DB_PREFIX_.'employee.id_employee AND id_profile = 10 AND '._DB_PREFIX_.'commande_livreur.id_order = '.$commande['id_order'];
                $livreur = Db::getInstance()->ExecuteS($sql);

                $text .= '- Commande n°' . $commande['id_order'] . " (Réf. " . $commande['reference'] . ') à '. date('H:i:s', strtotime($commande['date_add'])) .' : <strong>(Livreur assigné : '.$livreur[0]['firstname'].' '.$livreur[0]['lastname'].')</strong><br>';

                // Récupération des articles de la commande assignée
                $sql = 'SELECT * FROM '._DB_PREFIX_.'order_detail WHERE id_order = '. $commande['id_order'];

                $totalCommande = 0.0; // Montant total de la commande assignée

                // S'il y a au moins un article
                if ($articles = Db::getInstance()->ExecuteS($sql))
                    foreach ($articles as $article)
                        $text .= "<span style='padding-left: 20px;'></span>* ".$article['product_quantity'].'x '.$article['product_name']. " (" . number_format(($article['unit_price_tax_incl']*$article['product_quantity']), 2). "€)<br>";
                else
                    $text .= "Erreur lors de l'affichage de l'article";

                $totalCommande += number_format($commande['total_products'], 2);

                $text .= "<span style='padding-left: 8px;'></span>Total : ".$totalCommande."€<br>". (!next( $commandes ) ? "" : "<br>" );
            }
        } else {
            $text .= "Aucune commande assignée.";
        }

        $text .= "</div>";
        return $text;
    }

    public function totalCaisseLivreurs()
    {
        $text = "<div class='panel'><div class='panel-heading'>Total de caisse pour chaque livreur assigné</div>";

        // Récupération des livreur assignés à au moins une commande ainsi que le total de ses commandes
        $sql = "SELECT "._DB_PREFIX_."employee.lastname, "._DB_PREFIX_."employee.firstname, SUM("._DB_PREFIX_."order_detail.product_price) as total 
                FROM "._DB_PREFIX_."order_detail, "._DB_PREFIX_."orders, "._DB_PREFIX_."commande_livreur 
                JOIN "._DB_PREFIX_."employee ON "._DB_PREFIX_."employee.id_employee = "._DB_PREFIX_."commande_livreur.id_employee 
                WHERE "._DB_PREFIX_."orders.id_order IN (SELECT id_order FROM "._DB_PREFIX_."commande_livreur) 
                AND date_add BETWEEN '2020-07-20 00:00:00' AND '2020-07-20 23:59:59' 
                AND "._DB_PREFIX_."orders.id_order = "._DB_PREFIX_."order_detail.id_order 
                AND "._DB_PREFIX_."commande_livreur.id_order = "._DB_PREFIX_."orders.id_order 
                GROUP BY "._DB_PREFIX_."commande_livreur.id_employee";

        $total = 0.0; // Total des caisses

        // S'il y a au moins une caisse
        if ($livreurs = Db::getInstance()->ExecuteS($sql)){
            foreach ($livreurs as $livreur) {
                $text .= "- ". $livreur['firstname']." ".$livreur['lastname']." : ".number_format($livreur['total'],2)."€<br>";
                $total += $livreur['total'];
            }
            $text .= "<br><strong>Total des caisses : ".number_format($total,2)."€</strong>";
        } else {
            $text .= "Aucun caisse disponible.";
        }

        $text .= "</div>";
        return $text;
    }
}